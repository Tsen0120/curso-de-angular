import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-controle',
  templateUrl: './controle.component.html',
  styleUrls: ['./controle.component.css']
})
export class ControleComponent implements OnInit {
  @Output() comecarIntervalo = new EventEmitter<number>();
  intervalo;
  ultimoNumero = 0;
  constructor() { }

  ngOnInit(): void {
  }
  comecarJogo(){
    this.intervalo= setInterval(() => {
      this.comecarIntervalo.emit(this.ultimoNumero + 1);
      this.ultimoNumero ++;
    }, 1500);
  }
  pausarJogo(){
    clearInterval(this.intervalo);
  }
}
