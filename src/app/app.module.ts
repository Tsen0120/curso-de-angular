import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ControleComponent } from './controle/controle.component';
import { SoftComponent } from './soft/soft.component';

@NgModule({
  declarations: [
    AppComponent,
    ControleComponent,
    SoftComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
